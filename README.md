# Scope
Deploy Grafana/Loki to Ostree based OS using Podman.


# Notes Grafana
Grafana can be configured thru API, .ini or /etc/grafana/provisioning/ ... Alloy only getting deployed with Molecule.


# Next
- Deploy to Rpi05


# Backlog
- add ssl
- trigger update from template/molecule.git repos


# Debug Grafana
- http://192.168.122.251:3000/login ... DNS nok ... http://fcos-41:3000/login # admin/<bitwarden> > explore > Logs ... empty?
- $ podman exec -it grafana-server /bin/sh


# Debug Loki
- [http://fcos-41:3100/ready](http://fcos-40:3100/ready)
- http://fcos-41:3100/metrics
```
$ curl -s http://fcos-41:3100/metrics | grep 'loki_distributor_lines_received_total{'
```
